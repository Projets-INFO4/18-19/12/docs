# Log book
## 28/01/19 : 
Definition of the procedure to follow, the different stages of our project as well as the things to do the current day.
We started by installing OpenHab 2 and then we tried to install the IDE in order to run programs using OpenHab 2.
Unfortunately, we were unable to correct the errors that followed this installation.
Forecasts -> Correct the errors to be able to execute the code, go find the cameras to the fablab to understand how they work.

    
## 04/02/19 : 
Contact established with former project collaborators.
Multiple attempts to install openHab2 but none of them have worked.
    
## 11/02/19 : 
Successful installation of Eclipse SmartHome thanks to this link https://www.eclipse.org/smarthome/documentation/development/ide.html, removal of compilation errors.
Search for issues given in the git page and localization of these errors in the code of the former project.
Getting the equipment back in order to start testing the previous code with the cameras we will retrieve next week.
    
## 17/02/19 :
We found the repository we will mainly have to modify :
https://github.com/lawchris/openhab2-addons/tree/dlink-upnpcamera-binding/addons/binding/org.openhab.binding.dlinkupnpcamera.
    
## 18/02/19 :
Internet researches about UPnP.
Code review and attempt to connect the cameras to our computers.
Communication with former students to understand better the architecture of the code.
    
## 04/03/19 :
Recovery of all the material to connect cameras to our computers.
We get some errors after importing previous years students' program.
We decided to take some time to understand the OSGI's principle because the teacher told us that it could be the key.
    
## 05/03/19 :
Reading of some documents and of the following website based on Didier Donsez's website : http://davidmasclet.gisgraphy.com/post/2009/11/26/10-minutes-pour...comprendre-OSGI in order to understand OSGI and bundles concepts.
We also try to understand Maven, how it works,  what is it used for, how tu build a project etc.
    
## 11/03/19 :
Addition of the binding org.openhab.binding.dlinkupnpcamera to the project Openhab, and addition of this binding in the pom.xml files.
We tried to create the .jar project by building it with Maven.
Once the .jar project was created we tried to add it in Openhab in order to finally be able to detect the cameras in Openhab.
Unfortunatly while executing bundle:list we noticed that the bundle was "installed" but still not "active".
To activate it we tried `bundle:start 198` (198 was the ID of this bundle).
Unfortunatly we get some errors informing us that some features were missing.
We achieved to add some features by using the command : `feature:install openhab-transport-upnp`
A feature is still missing.

## 12/03/19 :
To finally activate the bundle in openHAB it was nedeed to install the feature named openhab-runtime-compat1x by doing `feature:install openhab-runtime-compat1x`.
Then `bundle:start 200` and the bundle was finally active.
Unfortunatly it worked for only one member of the group, other members had the bundle waiting and not active.
We also started to create and prepare our mid-term presentation.

## 18/03/19 :
We finished to prepare the presentation and finally tried to connect cameras via openHab, because the bundle was active.
We launched the research of "Things" (upnp device), but openHbab wasn't able to find any camera, even when we tried to enter cameras manually.

## 19/03/19 :
During the presentation we talked about git. Indeed after our modification, we will have to make another pull-request. But we cloned the repository whereas we had to fork it.
So, while some members tried to detect the cameras on openHab, the others forked the project properly.

## 25/03/19 :
The bundle is still waiting but it is impossible to resolve the problem and make it active.
Even if it doesn't work we started to correct the requirements made by Opnehab community on this page 'https://github.com/openhab/openhab2-addons/pull/2111'.

## 26/03/19:
We divided the group in two parts. One member still tried to make the bundle active while the others continued to resolve comments made by OpenHAB comunity.
To make the bundle active, we finally had to modify the manifest and remove all the exported packages. Then we did a maven clean install of the all project.
We corrected the first pull requests, the easiest one.

## 26/03/19:
The bundle is now active, so we tried to detect it by using OpenHAB, it failed. So we tried to detect it thanks to Apache Felix, and it finally worked only Didier Donsez Computer.
We had to reset the camera because we didn't know the password.
Unfortunately, we are unable to reproduce the same results on our computers.
In parallel we corrected all the comments of the pull request.